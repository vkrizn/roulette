package si.atei.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Player implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(Player.class);
	
	private Game game;
	private String name;
	private int money;
	private List<Bet> bets = new ArrayList<Bet>();
	private Boolean selected = false;
	
	public int getMoney() {
		return money;
	}
	
	public void setMoney(int money) {
		this.money = money;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public void setSelected(Boolean selected)
	{
		this.selected = selected;
	}
	
	public String getSelectedClass()
	{
		return (this.selected ? "selectedPlayer" : "");
	}
	
	public List getBets()
	{
		return bets;
	}
	
	/**
	 * Place a bet
	 * @param b
	 */
	public void placeBet(Bet b) {
		if(this.money > 0)
		{
			bets.add(b);
			// TODO handle negative balance after bet
			this.money -= b.getMoney();
			log.debug("{} placed bet {}", name, b);
		}
	}
	
	/**
	 * Play a round of roulette
	 */
	public void playRound() {
		int number = game.getNumber();
		for (Bet b : bets) {
			int winnigs = b.playRound(number);
			if (winnigs == 0) {
				log.debug("{} lost bet {}", name, b);
			}
			else {
				setMoney(getMoney() + winnigs);
				log.debug("{} won {} on bet {}", new Object[]{name, winnigs, b});
			}
		}
		bets.clear();
		log.debug("{} now has {}", name, money);
	}
}
