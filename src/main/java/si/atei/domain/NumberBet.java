package si.atei.domain;

public class NumberBet extends Bet {

	private int number;

	public NumberBet(int money, int number) {
		super(money);
		this.number = number;
	}

	@Override
	protected int odds() {
		return 36;
	}
	
	@Override
	protected boolean won(int number) {
		return number == this.number;
	}

	@Override
	public String toString() {
		return super.toString() + " on number " + number;
	}
}
