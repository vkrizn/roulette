package si.atei.domain;

public class HighLowBet extends Bet {

	private Boolean high;

	public HighLowBet(int money, Boolean high) {
		super(money);
		this.high = high;
	}

	@Override
	protected int odds() {
		return 2;
	}
	
	@Override
	protected boolean won(int number) {
		return ((number > 18) == high) && number != 0;
	}

	@Override
	public String toString() {
		return super.toString() + " on " + (high ? "high" : "low");
	}
}