package si.atei.domain;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuletteField implements Serializable {

	private static enum Colour {
		Red, Black;

		public String getPrettyName() {
			return this.name().toLowerCase();
		}
	}
	
	private static final Logger log = LoggerFactory.getLogger(RuletteField.class);
	
	private final int number;
	private final Colour colour;
	
	public RuletteField(int number)
	{
		this.number = number;
		
		if(number <= 10 || number >=19 && number <=28)
			colour = (number % 2 == 0 ? Colour.Black : Colour.Red);
		else
			colour = (number % 2 == 0 ? Colour.Red : Colour.Black);
	}
	
	public int getNumber()
	{
		return number;
	}
	
	public Colour getColour()
	{
		return colour;
	}
	
}
