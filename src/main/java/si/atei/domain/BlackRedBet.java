package si.atei.domain;

public class BlackRedBet extends Bet {

	private Boolean black;

	public BlackRedBet(int money, Boolean black) {
		super(money);
		this.black = black;
	}

	@Override
	protected int odds() {
		return 2;
	}
	
	@Override
	protected boolean won(int number) {
		if(number == 0)
			return false;
		else if(number <= 10 || number >=19 && number <=28)
			return (number % 2 == 0 ? black == true : black == false);
		else
			return (number % 2 == 0 ? black == false : black == true);
	}

	@Override
	public String toString() {
		return super.toString() + " on " + (black ? "black" : "red");
	}
}