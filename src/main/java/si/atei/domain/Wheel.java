package si.atei.domain;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a wheel on the roulette table 
 *  
 */
public class Wheel implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(Wheel.class);
	
	private Integer result;

	public void spin() {
		result = (int)(Math.random() * 37);
		log.debug("spin: {}", result);
	}
	
	public Integer getResult() {
		return result;
	}
	
}
