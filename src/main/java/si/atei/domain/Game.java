package si.atei.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;

public class Game implements Serializable {

	private Wheel wheel = new Wheel();
	private Set<Player> players = new LinkedHashSet<Player>();
	private Player selectedPlayer = null;
	private Component previousSelected = null;
	
	public Set<Player> getPlayers() {
		return players;
	}
	
	public void selectPlayer(Player player)
	{
		for (Player p : players) {
			p.setSelected(player.equals(p));
		}
		selectedPlayer = player;
	}
	
	public Player getSelectedPlayer()
	{
		return selectedPlayer;
	}
	
	public Component getSelected()
	{
		return previousSelected;
	}
	
	public void setSelected(Component playerLabel)
	{
		previousSelected = playerLabel;
	}	//*/
	
	public void setPlayers(Set<Player> players) {
		this.players = players;
	}

	public void addPlayer(String name, int money) {
		Player p = new Player();
		p.setMoney(money);
		p.setName(name);
		p.setGame(this);
		players.add(p);
	}
	
	/**
	 * Play a round of roulette.
	 * 
	 * Spin the wheel and process all players' bets
	 * 
	 */
	public void playRound() {
		wheel.spin();
		for (Player p : players) {
			p.playRound();
		}
	}

	/**
	 * Number on the wheel
	 * 
	 * @return
	 */
	public int getNumber() {
		return wheel.getResult();
	}
	
}
