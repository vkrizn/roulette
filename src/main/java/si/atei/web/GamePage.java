package si.atei.web;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import si.atei.domain.*;

public class GamePage extends WebPage {
	private static final long serialVersionUID = 1L;
	private Set<PlayerPage> playerControls = new LinkedHashSet<PlayerPage>();
	
	public GamePage(final PageParameters parameters) {

    	final WebMarkupContainer playerContainer = new WebMarkupContainer("playerContainer");
    	playerContainer.setOutputMarkupId(true);
		add(playerContainer);
		
		add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
    	
    	// Populate roulette fields TODO: Make ajax betting
    	add(new RefreshingView<RuletteField>("fields") {
			@Override
			protected Iterator<IModel<RuletteField>> getItemModels() {
				final Set<RuletteField> fields = new LinkedHashSet<RuletteField>();
				for (int n = 1; n < 37; n++)
					fields.add(new RuletteField(n));
				return new DefaultModelIterator(fields);
			}

			@Override
			protected void populateItem(final Item<RuletteField> item) {
				final Label number = new Label("number", new PropertyModel<String>(item.getModel(), "number"));
				AjaxLink<RuletteField> link;
				item.add(link = new AjaxLink<RuletteField>("numberLink", item.getModel()){
					public void onClick(AjaxRequestTarget target) {
		                // add the components that need to be updated to 
		                // the target
						if(getGame().getSelectedPlayer() != null)
						{
							getGame().getSelectedPlayer().placeBet(new NumberBet(100, item.getModelObject().getNumber()));
							target.add(getGame().getSelected());
							//target.add(getGame().getPreviousSelected());
			            }
					}
				});
				link.add(new AttributeAppender("class", new PropertyModel<String>(item.getModel(), "colour.prettyName"), " "));
				link.add(number);
			}
		});
		
    	playerContainer.add(new RefreshingView<Player>("players") {
			@Override
			protected Iterator<IModel<Player>> getItemModels() {
				return new DefaultModelIterator(getGame().getPlayers());
			}

			@Override
			protected void populateItem(final Item<Player> item) {
				PlayerPage playerPage = new PlayerPage("player", item.getModel());
				playerControls.add(playerPage);
				item.add(playerPage);
			}
    	});
		
		final Label spinNumber = new Label("spinNumber", new PropertyModel<Integer>(getDefaultModelObject(), "number"));
		spinNumber.setOutputMarkupId(true);
		playerContainer.add(spinNumber);
		
		add(new AjaxLink("evenBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new OddEvenBet(100, true));
					target.add(getGame().getSelected());
	            }
            }
		});
		
		add(new AjaxLink("oddBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new OddEvenBet(100, false));
					target.add(getGame().getSelected());
	            }
            }
		});

		add(new AjaxLink("blackBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new BlackRedBet(100, true));
					target.add(getGame().getSelected());
	            }
            }
		});
		
		add(new AjaxLink("redBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new BlackRedBet(100, false));
					target.add(getGame().getSelected());
	            }
            }
		});
		
		add(new AjaxLink("zeroBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new NumberBet(100, 0));
					target.add(getGame().getSelected());
	            }
            }
		});

		add(new AjaxLink("highBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new HighLowBet(100, true));
					target.add(getGame().getSelected());
	            }
            }
		});
		
		add(new AjaxLink("lowBetClick") {
			public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
				if(getGame().getSelectedPlayer() != null)
				{
					getGame().getSelectedPlayer().placeBet(new HighLowBet(100, false));
					target.add(getGame().getSelected());
	            }
            }
		});
				
		playerContainer.add(new AjaxLink("spinLink") {
            public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target
            	getGame().playRound();
            	for(PlayerPage pp : playerControls)
            		target.add(pp);
            	target.add(spinNumber);
            }
        });
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		Game game = new Game();
		game.addPlayer("Pepe Morales", 1000);
		game.addPlayer("Texas Bob", 2000);
		game.addPlayer("Lola", 1000);
		setDefaultModel(Model.of(game));
	}
	
	public Game getGame() {
		return (Game) getDefaultModelObject();
	}
}
