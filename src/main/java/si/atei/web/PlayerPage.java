package si.atei.web;

import java.util.Iterator;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import si.atei.domain.Bet;
import si.atei.domain.Player;

public class PlayerPage extends Panel {

	public PlayerPage(String id, final IModel<Player> model)
	{
		super(id, model);
		
		final Panel pan = this;
		this.setOutputMarkupId(true);
		
		AjaxLink<Player> link;
		add(link = new AjaxLink<Player>("playerLink", model) {
            public void onClick(AjaxRequestTarget target) {
                // add the components that need to be updated to 
                // the target		            	
            	model.getObject().getGame().selectPlayer(this.getModelObject());
            	target.add(pan);
            	if(model.getObject().getGame().getSelected() != null)
            		target.add(model.getObject().getGame().getSelected());	//*/
            	model.getObject().getGame().setSelected(pan);
            }
        });
		add(new AttributeAppender("class", new PropertyModel<String>(model, "selectedClass"), " "));
		link.add(new Label("name", new PropertyModel<String>(model, "name")));
		add(new Label("money", new PropertyModel<Integer>(model, "money")));
		
		if(model.getObject().getBets() != null)
		{
			add(new RefreshingView<Bet>("bets") {
				@Override
				protected Iterator<IModel<Bet>> getItemModels() {
					return new DefaultModelIterator(model.getObject().getBets());
				}

				@Override
				protected void populateItem(final Item<Bet> item) {
					item.setOutputMarkupId(true);
					item.add(new Label("bet", new PropertyModel<String>(item.getModel(), "toString")));
				}
			});
		}
	}
}