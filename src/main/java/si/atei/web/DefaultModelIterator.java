package si.atei.web;

import java.io.Serializable;

import org.apache.wicket.markup.repeater.util.ModelIteratorAdapter;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public final class DefaultModelIterator<T extends Serializable> extends ModelIteratorAdapter<T> {
	DefaultModelIterator(Iterable<T> iterable) {
		super(iterable);
	}

	@Override
	protected IModel<T> model(T object) {
		return Model.of(object);
	}
}